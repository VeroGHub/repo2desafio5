package com.javatpoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class SpringbootDesafio5Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDesafio5Application.class, args);
	}

}
